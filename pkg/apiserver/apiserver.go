package apiserver

import (
	"context"
	"github.com/juju/loggo"
	"github.com/soheilhy/cmux"
	"gitlab.com/mvenezia/discord-service/pkg/util/log"
	"google.golang.org/grpc"
	"net/http"
	"strconv"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	service "gitlab.com/mvenezia/discord-service/internal/discord-service"
	pb "gitlab.com/mvenezia/discord-service/pkg/generated/api"
	"gitlab.com/mvenezia/discord-service/pkg/ui/website"
	"google.golang.org/grpc/reflection"
)

var (
	logger loggo.Logger
)

type ServerOptions struct {
	PortNumber int
}

func AddServersToMux(tcpMux cmux.CMux, options *ServerOptions) {
	logger = log.GetModuleLogger("pkg.apiserver", loggo.INFO)
	addGRPCServer(tcpMux)
	addRestAndWebsite(tcpMux, options.PortNumber)
}

func addGRPCServer(tcpMux cmux.CMux) {
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	pb.RegisterDiscordServiceAPIServer(grpcServer, newgRPCServiceServer())
	reflection.Register(grpcServer)

	grpcListener := tcpMux.MatchWithWriters(cmux.HTTP2MatchHeaderFieldPrefixSendSettings("content-type", "application/grpc"))
	// Start servers
	go func() {
		logger.Infof("Starting gRPC Server")
		if err := grpcServer.Serve(grpcListener); err != nil {
			logger.Criticalf("Unable to start external gRPC server")
		}
	}()
}

func addRestAndWebsite(tcpMux cmux.CMux, grpcPortNumber int) {
	httpListener := tcpMux.Match(cmux.HTTP1Fast())

	go func() {
		router := http.NewServeMux()
		website.AddWebsiteHandles(router)
		addgRPCRestGateway(router, grpcPortNumber)
		addCORSHeader := func(h http.Handler) http.Handler {
			return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.Header().Add("Access-Control-Allow-Origin", "*")
				h.ServeHTTP(w, r)
			})
		}
		httpServer := http.Server{
			Handler: addCORSHeader(router),
		}
		logger.Infof("Starting HTTP/1 Server")
		httpServer.Serve(httpListener)
	}()

}

func addgRPCRestGateway(router *http.ServeMux, grpcPortNumber int) {
	dopts := []grpc.DialOption{grpc.WithInsecure()}
	gwmux := runtime.NewServeMux()
	pb.RegisterDiscordServiceAPIHandlerFromEndpoint(context.Background(), gwmux, "localhost:"+strconv.Itoa(grpcPortNumber), dopts)
	router.Handle("/api/", gwmux)
}

func newgRPCServiceServer() *service.Server {
	return new(service.Server)
}
