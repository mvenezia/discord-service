// Code generated by go-bindata.
// sources:
// assets/generated/swagger/api.swagger.json
// DO NOT EDIT!

package swaggerjson

import (
	"github.com/elazarl/go-bindata-assetfs"
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func bindataRead(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	clErr := gz.Close()

	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}
	if clErr != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

type asset struct {
	bytes []byte
	info  os.FileInfo
}

type bindataFileInfo struct {
	name    string
	size    int64
	mode    os.FileMode
	modTime time.Time
}

func (fi bindataFileInfo) Name() string {
	return fi.name
}
func (fi bindataFileInfo) Size() int64 {
	return fi.size
}
func (fi bindataFileInfo) Mode() os.FileMode {
	return fi.mode
}
func (fi bindataFileInfo) ModTime() time.Time {
	return fi.modTime
}
func (fi bindataFileInfo) IsDir() bool {
	return false
}
func (fi bindataFileInfo) Sys() interface{} {
	return nil
}

var _apiSwaggerJson = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\xa4\x56\xcf\x6f\xdb\x3a\x0c\xbe\xe7\xaf\x20\xf4\xde\xf1\xbd\x3a\xed\x31\xa7\xf7\xb0\x02\x43\x30\x14\x2b\xb6\xa1\x3d\x0c\x45\xa0\xc8\x8c\xad\xce\x16\x05\x89\x4e\xd7\x0d\xf9\xdf\x07\xda\xaa\xed\xfc\x70\x37\xa4\xb7\x44\x24\x3f\x92\x1f\x3f\x51\xfe\x39\x03\x50\xf1\x49\x17\x05\x06\xb5\x00\x75\x75\x31\x57\xff\xc8\x99\x75\x1b\x52\x0b\x10\x3b\x80\x62\xcb\x15\x8a\xfd\xda\x46\x43\x21\x87\xcf\x18\xb6\xd6\x20\xfc\x7f\xbb\x6c\xfd\x01\xd4\x16\x43\xb4\xe4\xc4\x6b\x3b\xbf\xb8\x4c\x40\x00\xca\x90\x63\x6d\xb8\x47\x03\x50\x4e\xd7\x2d\xdc\x8d\x35\xa5\xc6\x0a\xee\xd0\xe1\x0f\xab\x53\x04\x80\x6a\x42\x25\xf6\x92\xd9\xc7\x45\x96\x15\x96\xcb\x66\x7d\x61\xa8\xce\xb6\x87\xae\x58\x6b\xdb\x3a\xd7\xc9\xf4\x5f\x21\x27\xe2\xac\x5a\x9f\xdd\x0c\x60\xd7\x36\x15\x4d\x89\x35\x46\xb5\x80\xaf\x5d\x69\x2d\xfe\x4b\x9d\xf2\x47\x22\x1e\x5a\x5f\x43\x2e\x36\x7b\xce\xda\xfb\xca\x1a\xcd\x96\x5c\xf6\x18\xc9\x0d\xbe\x3e\x50\xde\x98\x3f\xf4\xd5\x5c\xc6\x81\xd9\x4c\x7b\x9b\x6d\x2f\xb3\x81\xbd\x9e\xa4\x02\xc7\x9c\x49\xf9\x4d\x5d\xeb\xf0\x2c\xbd\xde\xdb\xaa\x82\x80\xdc\x04\x07\x29\x14\x64\x64\xa1\x6e\x73\x82\x5e\x53\xc3\xa0\xbd\x85\x88\x61\x8b\xa1\xa7\x0b\x40\x91\xc7\xd0\x7a\x2d\x73\x81\x7a\x8f\x7c\xd7\x21\x2c\x07\x80\xb1\x7f\xc0\xe8\xc9\x45\x8c\x7b\xd5\x00\xa8\xab\xf9\xfc\xe0\x08\x40\xe5\x18\x4d\xb0\x9e\x93\x14\x46\x40\x5d\x0f\x32\x02\x7d\x14\x06\xa0\xfe\x0e\xb8\x91\x88\xbf\xb2\x1c\x37\xd6\x59\x41\x88\x59\xde\x09\x2e\x76\x7a\x1b\x6a\xfd\x84\xbe\x7a\x56\x7b\x18\xbb\xd9\xa9\xdf\xbb\x51\x27\xac\x8b\x61\x46\xe9\x2c\x29\x3a\x09\x5a\xf4\xdc\x5b\x1f\x66\x63\xb0\x41\x46\xa3\xfa\x86\x41\x1e\x94\x76\x82\xd2\xd1\x68\xf9\xd9\xb7\xfa\xa7\xf5\x23\x1a\x1e\xb4\xec\x83\x0c\x87\xed\x01\xd7\xaa\xb0\xbc\x3a\x56\xc8\x1e\x54\xe4\x60\x5d\xb1\x47\xf7\x70\x6d\xbf\x94\x08\xac\x0b\x20\x07\x5c\x22\x14\x96\x21\xa0\xa7\x68\x99\xc2\x88\xc5\x31\x57\x92\xd2\x50\x5d\x5b\x3e\x3b\x63\xa9\x63\x09\xb4\xe9\x53\x26\xb8\xc9\x74\x1c\x10\x57\x91\x35\xe3\x59\x29\xef\x4b\xe4\x12\x03\x50\x00\x47\xdc\x66\x15\x44\x78\xd2\x11\x4c\x85\xda\xc1\x53\x89\x0e\xd6\x8d\xad\x26\x8a\x10\x53\xbe\xca\xcf\x2d\xe0\x5a\x33\x4a\xbf\x2d\xcc\x44\x9b\xf4\xa6\x39\x26\x55\x49\x92\x82\xa0\x89\x98\x03\x93\xf0\xea\x6d\x85\xa7\x33\x26\x63\x38\x2b\xdf\xbb\x14\xdc\xa6\x3a\x8d\xef\x2b\xcd\xa2\xf1\xb3\xf0\x6f\x53\x30\x58\xee\xc6\xd4\xe5\xcb\x61\x43\x01\x32\x08\x8d\x73\xd6\x89\x6c\x47\xb9\xf7\x2f\x65\xda\xdd\xaf\xaf\x89\x37\xdc\x3c\xfa\x36\xd5\xd8\x9a\x48\x44\xb5\xdf\x59\x77\xdb\x27\xcd\x7d\xe3\xcb\x0d\xf4\x6b\xb8\xed\xfc\xe3\x87\xd3\xf4\x26\xb5\xac\xec\xc9\x45\x02\xd3\x8b\xf3\xf7\xfb\xe8\x75\x89\x8d\x3d\xa7\xc8\xef\x37\x22\x7e\x67\x0c\x4e\x57\xd7\x64\x46\x2b\xf1\xe0\x29\xb8\xa1\x80\xe9\x61\x4a\xe3\x82\x78\xfc\x19\x71\xfc\xf2\x57\xba\x7b\xf9\x5f\xde\xf7\x97\x37\xe1\xdf\x14\x2d\xe5\xed\x66\xbb\xd9\xaf\x00\x00\x00\xff\xff\xc2\x88\xf9\xca\xcc\x08\x00\x00")

func apiSwaggerJsonBytes() ([]byte, error) {
	return bindataRead(
		_apiSwaggerJson,
		"api.swagger.json",
	)
}

func apiSwaggerJson() (*asset, error) {
	bytes, err := apiSwaggerJsonBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "api.swagger.json", size: 2252, mode: os.FileMode(420), modTime: time.Unix(1550257957, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("Asset %s can't read by error: %v", name, err)
		}
		return a.bytes, nil
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// MustAsset is like Asset but panics when Asset would return an error.
// It simplifies safe initialization of global variables.
func MustAsset(name string) []byte {
	a, err := Asset(name)
	if err != nil {
		panic("asset: Asset(" + name + "): " + err.Error())
	}

	return a
}

// AssetInfo loads and returns the asset info for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func AssetInfo(name string) (os.FileInfo, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("AssetInfo %s can't read by error: %v", name, err)
		}
		return a.info, nil
	}
	return nil, fmt.Errorf("AssetInfo %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() (*asset, error){
	"api.swagger.json": apiSwaggerJson,
}

// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//     data/
//       foo.txt
//       img/
//         a.png
//         b.png
// then AssetDir("data") would return []string{"foo.txt", "img"}
// AssetDir("data/img") would return []string{"a.png", "b.png"}
// AssetDir("foo.txt") and AssetDir("notexist") would return an error
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		cannonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(cannonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for childName := range node.Children {
		rv = append(rv, childName)
	}
	return rv, nil
}

type bintree struct {
	Func     func() (*asset, error)
	Children map[string]*bintree
}
var _bintree = &bintree{nil, map[string]*bintree{
	"api.swagger.json": &bintree{apiSwaggerJson, map[string]*bintree{}},
}}

// RestoreAsset restores an asset under the given directory
func RestoreAsset(dir, name string) error {
	data, err := Asset(name)
	if err != nil {
		return err
	}
	info, err := AssetInfo(name)
	if err != nil {
		return err
	}
	err = os.MkdirAll(_filePath(dir, filepath.Dir(name)), os.FileMode(0755))
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(_filePath(dir, name), data, info.Mode())
	if err != nil {
		return err
	}
	err = os.Chtimes(_filePath(dir, name), info.ModTime(), info.ModTime())
	if err != nil {
		return err
	}
	return nil
}

// RestoreAssets restores an asset under the given directory recursively
func RestoreAssets(dir, name string) error {
	children, err := AssetDir(name)
	// File
	if err != nil {
		return RestoreAsset(dir, name)
	}
	// Dir
	for _, child := range children {
		err = RestoreAssets(dir, filepath.Join(name, child))
		if err != nil {
			return err
		}
	}
	return nil
}

func _filePath(dir, name string) string {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	return filepath.Join(append([]string{dir}, strings.Split(cannonicalName, "/")...)...)
}


func assetFS() *assetfs.AssetFS {
	assetInfo := func(path string) (os.FileInfo, error) {
		return os.Stat(path)
	}
	for k := range _bintree.Children {
		return &assetfs.AssetFS{Asset: Asset, AssetDir: AssetDir, AssetInfo: assetInfo, Prefix: k}
	}
	panic("unreachable")
}
