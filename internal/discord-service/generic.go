package discordservice

import (
	"github.com/juju/loggo"
	"gitlab.com/mvenezia/discord-service/pkg/util/log"
)

var (
	logger loggo.Logger
)

type Server struct{}

func SetLogger() {
	logger = log.GetModuleLogger("internal.discord-service", loggo.INFO)
}
