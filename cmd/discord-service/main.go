package main

import "gitlab.com/mvenezia/discord-service/cmd/discord-service/cmd"

func main() {
	cmd.Execute()
}
